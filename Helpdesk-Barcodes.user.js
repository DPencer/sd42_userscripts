// ==UserScript==
// @name         Helpdesk-Barcodes(DEPRECATED)
// @namespace    sd42.ca
// @version      0.1.3
// @description  This functionality has been rolled into the main Helpdesker. This script is safe to remove.
// @author       David Pencer
// @match        https://webhelpdesk.sd42.ca/helpdesk/WebObjects/Helpdesk.woa/wo/*
// @match        https://helpdesk.sd42.ca/helpdesk/WebObjects/Helpdesk.woa/wo/*
// @match        https://deo-helpdesk.mrpm.sd42.ca/helpdesk/WebObjects/Helpdesk.woa/wo/*
// @homepage	 https://gitlab.com/DPencer/sd42_userscripts
// @updateURL    https://gitlab.com/DPencer/sd42_userscripts/raw/master/Helpdesk-Barcodes.user.js
// @grant        none
// ==/UserScript==

(function() {
})();
