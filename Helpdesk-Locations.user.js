// ==UserScript==
// @name         Helpdesker
// @namespace    sd42.ca
// @version      0.3.6
// @description  Improves helpdesk with location numbers, better fonts, and barcodes on tickets; among other things.
// @author       David Pencer
// @match        https://webhelpdesk.sd42.ca/helpdesk/*
// @match        https://helpdesk.sd42.ca/helpdesk/*
// @match        https://deo-helpdesk.mrpm.sd42.ca/helpdesk/*
// @homepage     https://gitlab.com/DPencer/sd42_userscripts
// @updateURL    https://gitlab.com/DPencer/sd42_userscripts/raw/master/Helpdesk-Locations.user.js
// @grant        none
// ==/UserScript==

var locationTable = {'Harry Hooge Elementary': '36',
              'Blue Mountain Elementary': '41',
              'Fairview Elementary': '9',
              'Thomas Haney Secondary': '38',
              'Maple Ridge Elementary': '14',
              'Pitt Meadows Secondary': '3',
              'Kanaka Creek Elementary': '37',
              'Riverside Centre': '34',
              'Alexander Robinson Elementary': '42',
              'Hammond Elementary': '43',
              'Laity View Elementary': '32',
              'Maple Ridge Secondary': '1',
              'Samuel Robertson Tech': '45',
              'Pitt Meadows Elementary': '17',
              'District Education Office': '23',
              'Edith Mcdermott Elementary': '39',
              'Highland Park Elementary': '33',
              'Whonnock Elementary': '21',
              'Golden Ears Elementary': '11',
              'Glenwood Elementary': '10',
              'Alouette Elementary': '35',
              'Westview Secondary': '4',
              'Garibaldi Secondary': '2',
              'Websters Corners Elementary': '20',
              'Yennadon Elementary': '52',
              'Davie Jones Elementary': '31',
              'Eric Langton Elementary': '8',
              'Albion Elementary': '5',
              'Connex': '28',
              'Maintenance': '199',
              'cusqunela Elementary': '48',
              'Ridge Meadows College': '34',
//              'Student Support Services': 'SS',
//              'International Education': '@WSS',
//              'Continuing Education': '@RSC',
//              'Aboriginal Education': 'AB',
             };

// MAIN
(function() {
    'use strict';
    var title = document.title.trim()

    // Ticket listings / searches
    if (title.includes("Tickets")){
        locationNumsListing();
    }
    // Ticket detail pages
    else if (title.match(/Ticket \d{5,6} Details/)){
        locationNumsDropdown();
        passwordFonts();
        unlockScrolling();
    }
    // Ticket print pages
    else if(title == "Print View"){
        locationNumsPrint();
        passwordFonts();
        doBarcodes();
    }

})();

// Add location numbers to Ticket drop-down lists
function locationNumsDropdown(){
    var locations = document.querySelector('[id^="TicketLocation"]');
    if(locations){
        for (let i of locations){
            if(locationTable[i.innerHTML]){
                i.innerHTML = i.innerHTML + " - " + locationTable[i.innerHTML];
            }
        }
    }
}

// Add location numbers to listings of tickets (ie. Searches)
function locationNumsListing(){
    var header = document.querySelector("tr.header");
    if(header){
        var locationColumnOffset = 0;
        var j;
        for (locationColumnOffset = 0; j = header.cells[locationColumnOffset]; locationColumnOffset++){
            var th_content = j.querySelector("th")
            if(th_content && th_content.innerHTML.trim() == "Location"){
                locationColumnOffset++;
                break;
            }
        }
        var rows = document.querySelectorAll('[id^="row_"]');
        if(rows){
            for (let row of rows){
                var elem = row.querySelector("td:nth-child(" + (locationColumnOffset) + ") > div");
                var content = elem.innerHTML.trim()
                if(elem && locationTable[content]){
                    elem.innerHTML = locationTable[content] + " - " + content;
                }
            }
        }
    }
}

// Add location # to printed tickets
function locationNumsPrint(){
    var fields = document.querySelectorAll('td.data');
    if(fields){
        for (let field of fields){
            var contents = field.innerHTML.trim();
            if(locationTable[contents]){
                field.innerHTML = locationTable[contents] + ' - ' + contents;
            }
        }
    }
}

// Set better fonts for passwords in tickets
function passwordFonts(){
    var pw_tag = "District Email  Password" // Search string for password fields
    var pw_font = "Roboto Mono"
    // Add Google Font header
    var head = document.getElementsByTagName('head')[0];
    var linktag = document.createElement('link');
    linktag.href = "https://fonts.googleapis.com/css?family=" + pw_font;
    linktag.rel = "stylesheet";
    head.appendChild(linktag);
    // Get field on printed pages
    var labels = document.querySelectorAll('td[class^="label"]');
    if(labels){
        for (let field of labels){
            var pw_data = false;
            var label = field.innerHTML.trim();
            // On print pages
            if(label == pw_tag + ":") pw_data = field.nextElementSibling;
            // On ticket pages
            else if (label == pw_tag) pw_data = field.nextElementSibling.querySelector('input');
            if(pw_data){
                pw_data.style.fontFamily = pw_font;
                pw_data.style.fontSize = "larger";
            }
        }
    }
}

// Adds ticket number barcodes to printed pages
function doBarcodes(){
    var ticketno = document.querySelector("[class^='label']").innerHTML.split(":")[1];
    ticketno = parseInt(ticketno);
    var barcode = "<svg id='barcode'/>";

    // ## Above ticket #
    var tablehandle = document.querySelector("body > table:nth-child(1)");
    var newrow = tablehandle.insertRow(1).insertCell(0);
    newrow.classList.add("label");
    newrow.innerHTML = barcode;

    var scripturl = "https://cdn.jsdelivr.net/jsbarcode/3.3.20/JsBarcode.all.min.js"
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = scripturl;
    script.onload = function() {
        JsBarcode('#barcode', ticketno, {
            format: "CODE128",
            width: 1,
            height: 20,
            displayValue: false,
            margin: 0
        })
    }
    head.appendChild(script);
}

// Allow scrolling on already-being-edited pages.
function unlockScrolling(){
    document.body.style.overflow = "auto"
}